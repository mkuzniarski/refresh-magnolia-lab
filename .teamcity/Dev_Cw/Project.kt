package Dev_Cw

import Dev_Cw.buildTypes.*
import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.Project

object Project : Project({
    id("Dev_Cw")
    name = "cw"

    buildType(Dev_Cw_1)

    params {
        param("env", "uat/dev/cw")
    }
})
