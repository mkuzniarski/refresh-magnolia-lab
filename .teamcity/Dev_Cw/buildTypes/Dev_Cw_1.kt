package Dev_Cw.buildTypes

import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.script

object Dev_Cw_1 : BuildType({
    templates(_Self.buildTypes.MagnoliaRefreshTemplate)
    id("Dev_Cw")
    name = "uk"

    steps {
        script {
            id = "RUNNER_31"
            workingDir = "ansible-cc"
            scriptContent = """
                ansible="ansible-playbook plays/magnolia/restore-clean.yml \
                -i inventory/%env% \
                -e country=%system.teamcity.buildConfName% \
                -e teamcity_buildId=%teamcity.build.id% \
                -e teamcity_buildTypeId=%system.teamcity.buildType.id% \
                -D \
                -u ansible --private-key ansible-ssh-key.key"
                echo "${'$'}ansible"
                eval "${'$'}ansible"
            """.trimIndent()
        }
    }
})
