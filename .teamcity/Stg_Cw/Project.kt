package Stg_Cw

import Stg_Cw.buildTypes.*
import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.Project

object Project : Project({
    id("Stg_Cw")
    name = "cw"

    buildType(Stg_Cw_Uk)

    params {
        param("env", "stg/cw")
    }
})
