package Dev

import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.Project

object Project : Project({
    id("Dev")
    name = "uat_dev"

    subProject(Dev_Cci.Project)
    subProject(Dev_Regal.Project)
    subProject(Dev_Cw.Project)
})
