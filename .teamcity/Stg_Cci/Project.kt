package Stg_Cci

import Stg_Cci.buildTypes.*
import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.Project

object Project : Project({
    id("Stg_Cci")
    name = "cci"

    buildType(Stg_Cci_Hu)
    buildType(Stg_Cci_Ro)
    buildType(Stg_Cci_Pl)
    buildType(Stg_Cci_Cz)
    buildType(Stg_Cci_Rh)
    buildType(Stg_Cci_Sk)
    buildType(Stg_Cci_Il)

    params {
        param("env", "stg/cci")
        param("country", "%system.teamcity.buildConfName%")
    }
})
