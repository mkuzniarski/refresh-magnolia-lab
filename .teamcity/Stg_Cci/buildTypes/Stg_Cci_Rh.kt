package Stg_Cci.buildTypes

import jetbrains.buildServer.configs.kotlin.v2019_2.*

object Stg_Cci_Rh : BuildType({
    templates(_Self.buildTypes.MagnoliaRefreshTemplate)
    name = "rh"
})
