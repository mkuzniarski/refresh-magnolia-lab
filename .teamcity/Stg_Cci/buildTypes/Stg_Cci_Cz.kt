package Stg_Cci.buildTypes

import jetbrains.buildServer.configs.kotlin.v2019_2.*

object Stg_Cci_Cz : BuildType({
    templates(_Self.buildTypes.MagnoliaRefreshTemplate)
    name = "cz"
})
