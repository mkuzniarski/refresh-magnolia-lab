package Stg_Regal

import Stg_Regal.buildTypes.*
import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.Project

object Project : Project({
    id("Stg_Regal")
    name = "regal"

    buildType(Stg_Regal_Us)

    params {
        param("env", "stg/regal")
    }
})
