package Stg_Regal.buildTypes

import jetbrains.buildServer.configs.kotlin.v2019_2.*

object Stg_Regal_Us : BuildType({
    templates(_Self.buildTypes.MagnoliaRefreshTemplate)
    name = "us"
})
