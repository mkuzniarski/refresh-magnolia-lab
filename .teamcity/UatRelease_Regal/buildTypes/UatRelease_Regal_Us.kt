package UatRelease_Regal.buildTypes

import jetbrains.buildServer.configs.kotlin.v2019_2.*

object UatRelease_Regal_Us : BuildType({
    templates(_Self.buildTypes.MagnoliaRefreshTemplate)
    name = "us"
})
