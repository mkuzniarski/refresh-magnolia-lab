package UatRelease_Regal

import UatRelease_Regal.buildTypes.*
import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.Project

object Project : Project({
    id("UatRelease_Regal")
    name = "regal"

    buildType(UatRelease_Regal_Us)

    params {
        param("env", "uat/release/regal")
    }
})
