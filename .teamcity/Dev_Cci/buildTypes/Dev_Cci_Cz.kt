package Dev_Cci.buildTypes

import jetbrains.buildServer.configs.kotlin.v2019_2.*

object Dev_Cci_Cz : BuildType({
    templates(_Self.buildTypes.MagnoliaRefreshTemplate)
    name = "cz"
})
