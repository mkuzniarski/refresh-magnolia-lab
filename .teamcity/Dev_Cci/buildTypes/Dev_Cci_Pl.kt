package Dev_Cci.buildTypes

import jetbrains.buildServer.configs.kotlin.v2019_2.*

object Dev_Cci_Pl : BuildType({
    templates(_Self.buildTypes.MagnoliaRefreshTemplate)
    name = "pl"
})
