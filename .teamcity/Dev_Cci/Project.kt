package Dev_Cci

import Dev_Cci.buildTypes.*
import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.Project

object Project : Project({
    id("Dev_Cci")
    name = "cci"

    buildType(Dev_Cci_Pl)
    buildType(Dev_Cci_Cz)
    buildType(Dev_Cci_Rh)
    buildType(Dev_Cci_Il)
    buildType(Dev_Sk)
    buildType(Dev_Cci_Hu)
    buildType(Dev_Cci_Ro)

    params {
        param("env", "uat/dev/cci")
        param("country", "%system.teamcity.buildConfName%")
    }
})
