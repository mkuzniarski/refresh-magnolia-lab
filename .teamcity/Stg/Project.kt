package Stg

import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.Project

object Project : Project({
    id("Stg")
    name = "stg"

    subProject(Stg_Regal.Project)
    subProject(Stg_Cw.Project)
    subProject(Stg_Cci.Project)
})
