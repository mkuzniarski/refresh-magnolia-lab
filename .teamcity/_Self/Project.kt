package _Self

import _Self.buildTypes.*
import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.Project

object Project : Project({

    template(MagnoliaRefreshTemplate)

    params {
        param("country", "")
    }

    subProject(Stg.Project)
    subProject(Dev.Project)
    subProject(UatRelease.Project)
})
