package _Self.buildTypes

import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.script

object MagnoliaRefreshTemplate : Template({
    name = "magnolia refresh template"

    params {
        password("aws_access_key_id", "credentialsJSON:e5197473-697b-47b2-ab3b-a4b28271d619", display = ParameterDisplay.HIDDEN)
        param("cci-dev-bitbucket-ro-token", "UCNYZfLweC4HV3R9j2Sd")
        password("aws_secret_access_key", "credentialsJSON:449f3454-2969-4513-b4d5-8515835df477", display = ParameterDisplay.HIDDEN)
        password("ansible-ssh-key", "credentialsJSON:929461c5-85e6-40a9-b3b8-7cbf596bf08c", display = ParameterDisplay.HIDDEN)
        param("env", "default")
        param("env.ANSIBLE_CALLBACK_PLUGINS", "lib/callback_plugins")
        param("env.ANSIBLE_STDOUT_CALLBACK", "teamcity")
        password("ansible-vault", "credentialsJSON:5e2f413d-ecdb-4f75-ad78-5ef1096548f0", display = ParameterDisplay.HIDDEN)
    }

    steps {
        script {
            id = "RUNNER_30"
            scriptContent = """
                echo "== ansible inventory =="
                ansible-galaxy install -f -vvv -p . 'git+https://cci-dev:%cci-dev-bitbucket-ro-token%@bitbucket.org/cci-dev/ansible-cc'
                echo "== ansible role deployments =="
                ansible-galaxy install -f -vvv -p ansible-cc/roles 'git+https://cci-dev:%cci-dev-bitbucket-ro-token%@bitbucket.org/cci-dev/ansible-role-magnolia'
                mv ansible-cc/roles/ansible-role-magnolia ansible-cc/roles/magnolia
                echo "== ansible ssh =="
                echo "%ansible-ssh-key%" > ansible-cc/ansible-ssh-key.key
                chmod 400 ansible-cc/ansible-ssh-key.key
                echo "== ansible vault =="
                echo "%ansible-vault%" > /home/buildagent/.ansible-vpass
                echo "== aws creds =="
                echo "[default]\naws_access_key_id=%aws_access_key_id%\naws_secret_access_key=%aws_secret_access_key%" > /home/buildagent/.aws/credentials
                echo "== initialized variables =="
                echo "env: %env%"
                echo "country: %system.teamcity.buildConfName%"
            """.trimIndent()
        }
        script {
            id = "RUNNER_31"
            workingDir = "ansible-cc"
            scriptContent = """
                ansible="ansible-playbook plays/magnolia/restore-clean.yml \
                -i inventory/%env% \
                -e country=%country% \
                -e teamcity_buildId=%teamcity.build.id% \
                -e teamcity_buildTypeId=%system.teamcity.buildType.id% \
                -D \
                -u ansible --private-key ansible-ssh-key.key"
                echo "${'$'}ansible"
                eval "${'$'}ansible"
            """.trimIndent()
        }
    }
})
