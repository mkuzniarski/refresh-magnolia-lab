package UatRelease

import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.Project

object Project : Project({
    id("UatRelease")
    name = "uat_release"

    subProject(UatRelease_Cci.Project)
    subProject(UatRelease_Cw.Project)
    subProject(UatRelease_Regal.Project)
})
