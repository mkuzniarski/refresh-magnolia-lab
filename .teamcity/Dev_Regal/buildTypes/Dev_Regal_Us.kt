package Dev_Regal.buildTypes

import jetbrains.buildServer.configs.kotlin.v2019_2.*

object Dev_Regal_Us : BuildType({
    templates(_Self.buildTypes.MagnoliaRefreshTemplate)
    name = "us"
})
