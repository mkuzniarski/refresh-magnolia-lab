package Dev_Regal

import Dev_Regal.buildTypes.*
import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.Project

object Project : Project({
    id("Dev_Regal")
    name = "regal"

    buildType(Dev_Regal_Us)

    params {
        param("env", "uat/dev/regal")
    }
})
