package UatRelease_Cw.buildTypes

import jetbrains.buildServer.configs.kotlin.v2019_2.*

object UatRelease_Cw_Uk : BuildType({
    templates(_Self.buildTypes.MagnoliaRefreshTemplate)
    name = "uk"
})
