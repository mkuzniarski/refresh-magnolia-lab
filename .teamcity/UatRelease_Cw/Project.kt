package UatRelease_Cw

import UatRelease_Cw.buildTypes.*
import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.Project

object Project : Project({
    id("UatRelease_Cw")
    name = "cw"

    buildType(UatRelease_Cw_Uk)

    params {
        param("env", "uat/release/cw")
    }
})
