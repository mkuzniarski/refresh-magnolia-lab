package UatRelease_Cci

import UatRelease_Cci.buildTypes.*
import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.Project

object Project : Project({
    id("UatRelease_Cci")
    name = "cci"

    buildType(UatRelease_Cci_Hu)
    buildType(UatRelease_Cci_Ro)
    buildType(UatRelease_Cci_Il)
    buildType(UatRelease_Cci_Rh)
    buildType(UatRelease_Cci_Cz)
    buildType(UatRelease_Cci_Sk)
    buildType(UatRelease_Cci_Pl)

    params {
        param("env", "uat/release/cci")
        param("country", "%system.teamcity.buildConfName%")
    }
})
